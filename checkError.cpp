#include "checkError.h"

/// <summary>
/// Checks the display error.
/// </summary>
/// <param name="display">The display.</param>
/// <returns></returns>
int checkDisplayErr(ALLEGRO_DISPLAY *display)
{
	if (!display)
	{
		al_show_native_message_box(NULL, "DISPLAY-ERR", "Error:", "Problem with creat windows display", NULL, ALLEGRO_MESSAGEBOX_ERROR);

		return -1;
	}

	return 0;
}

/// <summary>
/// Checks the bitmap error.
/// </summary>
/// <param name="bitmap">The bitmap.</param>
/// <param name="name">The name of bitmap.</param>
/// <returns></returns>
int checkBitmapErr(ALLEGRO_BITMAP *bitmap, string name)
{
	if (!bitmap)
	{
		name = "Check png data: " + name;
		cout << endl << name << endl;
		al_show_native_message_box(NULL, "BITMAP-ERR", "Error:", name.c_str(), NULL, ALLEGRO_MESSAGEBOX_ERROR);

		return -1;
	}

	return 0;
}