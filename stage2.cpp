#include "stage2.h"

void Stage2::initStage(const int WINDOWWIDTH, const int WINDOWHEIGHT, bool * reload)
{
	initValuesCharacters(&this->enemy[0], WINDOWWIDTH - 50, 5, 50, 5, 50, true);
	initValuesCharacters(&this->enemy[1], WINDOWWIDTH - 450, WINDOWHEIGHT, 50, 5, 50, true);
	initValuesCharacters(&this->enemy[2], 100, 5, 50, 5, 50, true);

	this->enemySide1[0][0] = true;
	this->enemySide1[0][1] = false;
	this->enemySide1[0][2] = false;
	this->enemySide1[0][3] = false;

	this->enemySide1[2][0] = true;
	this->enemySide1[2][1] = false;
	this->enemySide1[2][2] = false;
	this->enemySide1[2][3] = false;

	this->enemySide1[1][0] = false;
	this->enemySide1[1][1] = true;
	this->enemySide1[1][2] = false;
	this->enemySide1[1][3] = false;

	this->enemy[0].timer = 0;
	hit[0] = false;

	this->enemy[1].timer = 60;
	hit[1] = false;

	this->enemy[2].timer = 30;
	hit[2] = false;

	if (*reload)
	{
		this->enemy[0].graphics[0] = al_load_bitmap("data/enemysTexturs/stage2/characterDown.png");
		this->enemy[0].graphics[1] = al_load_bitmap("data/enemysTexturs/stage2/characterDownHit.png");
		
		checkBitmapErr(this->enemy[0].graphics[0], "characterDown.png");
		checkBitmapErr(this->enemy[0].graphics[1], "characterDownHit.png");

		this->enemy[1].graphics[0] = al_load_bitmap("data/enemysTexturs/stage2/characterUp.png");
		this->enemy[1].graphics[1] = al_load_bitmap("data/enemysTexturs/stage2/characterUpHit.png");

		checkBitmapErr(this->enemy[1].graphics[0], "characterUp.png");
		checkBitmapErr(this->enemy[1].graphics[1], "characterUpHit.png");

		this->enemy[2].graphics[0] = al_load_bitmap("data/enemysTexturs/stage2/characterDown.png");
		this->enemy[2].graphics[1] = al_load_bitmap("data/enemysTexturs/stage2/characterDownHit.png");

		checkBitmapErr(this->enemy[2].graphics[0], "characterUp.png");
		checkBitmapErr(this->enemy[2].graphics[1], "characterUpHit.png");

		al_convert_mask_to_alpha(this->enemy[0].graphics[0], al_map_rgb(0, 0, 255));
		al_convert_mask_to_alpha(this->enemy[0].graphics[1], al_map_rgb(0, 0, 255));

		al_convert_mask_to_alpha(this->enemy[1].graphics[0], al_map_rgb(0, 0, 255));
		al_convert_mask_to_alpha(this->enemy[1].graphics[1], al_map_rgb(0, 0, 255));

		al_convert_mask_to_alpha(this->enemy[2].graphics[0], al_map_rgb(0, 0, 255));
		al_convert_mask_to_alpha(this->enemy[2].graphics[1], al_map_rgb(0, 0, 255));

		*reload = false;
	}

	this->jump[0] = true;
	this->jump[1] = true;
	this->jump[2] = true;
}

void Stage2::draw()
{
	for (int i = 0; i < 3; i++)
	{
		if (this->enemy[i].isLive)
		{
			if (!hit[i])
				al_draw_bitmap(this->enemy[i].graphics[0], this->enemy[i].pos_X, this->enemy[i].pos_Y, NULL);
			else
				al_draw_bitmap(this->enemy[i].graphics[1], this->enemy[i].pos_X, this->enemy[i].pos_Y, NULL);
			hit[i] = false;
		}
	}
}

//BARDZO BARDZO BRZYDKIE ALE DZIALA ;]
void Stage2::moves(int WINDOWWIDTH, int WINDOWHEIGHT, int posX, int posY)
{
	bool rush[3];
	int restX[3];
	int restY[3];

	int y1 = posY + 10;
	int y2 = posY + 20;
	int y3 = posY + 32;

	int x1 = posX + 10;
	int x2 = posX + 20;
	int x3 = posX + 32;

	for (int i = 0; i < 3; i++)
	{
		rush[i] = false;
		restX[i] = (this->enemy[i].pos_X % 100);
		restY[i] = (this->enemy[i].pos_Y % 100);

		if (this->enemy[i].health <= 0)
		{
			this->enemy[i].isLive = false;
			this->enemy[i].pos_X = -100;
			this->enemy[i].pos_Y = -100;
		}
	}

	for (int i = 0; i < 3; i++)
	{
		if (this->enemy[i].isLive)
		{
			if (restX[i] != 0)
			{
				jump[i] = false;

				if ((restX[i] % 5) != 0) this->enemy[i].pos_X -= (restX[i] % 5);
				else
				{
					if (this->enemy[i].pos_X <= 0)
					{
						this->enemy[i].pos_X += 5;
					}
					else if (this->enemy[i].pos_X > 0)
					{
						this->enemy[i].pos_X -= 5;
					}
				}
			}

			if (this->jump[i])
			{
				enemySide1[i][1] = true;

				if (this->enemy[i].pos_Y >= WINDOWHEIGHT - 50)
				{
					this->enemySide1[i][0] = false;		//down
					this->enemySide1[i][1] = true;		//up
					this->enemySide1[i][2] = false;		//left
					this->enemySide1[i][3] = false;		//right
				}
				else if (this->enemy[i].pos_Y <= 1)
				{
					this->enemySide1[i][0] = true;
					this->enemySide1[i][1] = false;
					this->enemySide1[i][2] = false;
					this->enemySide1[i][3] = false;
				}

				if (enemySide1[i][0])						//down
				{
					this->enemy[i].pos_Y += this->enemy[i].speed;
				}
				else if (enemySide1[i][1])					//up
				{
					this->enemy[i].pos_Y -= this->enemy[i].speed;
				}

				if (this->enemy[i].pos_Y % 100 == 0) jump[i] = false;
				else jump[i] = true;

			}
			else
			{
				if (this->enemy[i].timer >= 60)
				{
					this->jump[i] = true;
					this->enemy[i].timer = 0;
				}
				else
				{
					if (((y1 >= this->enemy[i].pos_Y) && (y1 <= this->enemy[i].pos_Y + 40)) ||
						((y2 >= this->enemy[i].pos_Y) && (y2 <= this->enemy[i].pos_Y + 40)) ||
						((y3 >= this->enemy[i].pos_Y) && (y3 <= this->enemy[i].pos_Y + 40)))
					{
						if (posX < this->enemy[i].pos_X)
						{
							enemySide1[i][0] = false;
							enemySide1[i][1] = false;
							enemySide1[i][2] = true;
							enemySide1[i][3] = false;
						}
						else if (posX > this->enemy[i].pos_X)
						{
							enemySide1[i][0] = false;
							enemySide1[i][1] = false;
							enemySide1[i][2] = false;
							enemySide1[i][3] = true;
						}
						rush[i] = true;
					}
					else if (((x1 >= this->enemy[i].pos_X) && (x1 <= this->enemy[i].pos_X + 40)) ||
						((x2 >= this->enemy[i].pos_X) && (x2 <= this->enemy[i].pos_X + 40)) ||
						((x3 >= this->enemy[i].pos_X) && (x3 <= this->enemy[i].pos_X + 40)))
					{
						if (posY < this->enemy[i].pos_Y)
						{
							enemySide1[i][0] = false;
							enemySide1[i][1] = true;
							enemySide1[i][2] = false;
							enemySide1[i][3] = false;
						}
						else if (posY > this->enemy[i].pos_Y)
						{
							enemySide1[i][0] = true;
							enemySide1[i][1] = false;
							enemySide1[i][2] = false;
							enemySide1[i][3] = false;
						}
						rush[i] = true;
					}

					if (rush[i])
					{
						if (enemySide1[i][0]) this->enemy[i].pos_Y += this->enemy[i].speed + 3;
						else if (enemySide1[i][1]) this->enemy[i].pos_Y -= this->enemy[i].speed + 3;
						else if (enemySide1[i][2]) this->enemy[i].pos_X -= this->enemy[i].speed + 3;
						else if (enemySide1[i][3]) this->enemy[i].pos_X += this->enemy[i].speed + 3;
						else
						{
							rush[i] = false;
							jump[i] = true;
						}
					}
					else
					{
						if (restY[i] != 0)
						{
							jump[i] = false;

							if ((restY[i] % 5) != 0) this->enemy[i].pos_Y -= (restY[i] % 5);
							else
							{
								if (this->enemy[i].pos_Y <= 0)
								{
									this->enemy[i].pos_Y += 5;
								}
								else if (this->enemy[i].pos_Y > 0)
								{
									this->enemy[i].pos_Y -= 5;
								}
							}
						}

						this->enemy[i].timer += 1;
					}
				}
			}
		}
	}

}

void Stage2::collisionsCharacterBulletsWithEnemys(int posX, int posY, bool * isLive, int dmg, bool *side)
{
	for (int i = 0; i < 3; i++)
	{
		if (((posX >= this->enemy[i].pos_X) && (posX <= this->enemy[i].pos_X + 40)) && ((posY >= this->enemy[i].pos_Y) && (posY <= this->enemy[i].pos_Y + 40)))
		{
			this->enemy[i].health -= dmg;
			*isLive = false;
			side[0] = false;
			side[1] = false;
			side[2] = false;
			side[3] = false;
			this->hit[i] = true;
		}
	}
}

void Stage2::collisionsEnemysWithCharacter(int * health, const int posX, const int posY)
{
	for (int i = 0; i < 3; i++)
	{
		if (((posX + 20 >= this->enemy[i].pos_X) && (posX + 20 <= this->enemy[i].pos_X + 40)) && ((posY + 20 >= this->enemy[i].pos_Y) && (posY + 20 <= this->enemy[i].pos_Y + 40)))
		{
			*health -= enemy[i].dmg;
			enemy[i].isLive = false;
		}
	}
}

bool Stage2::end()
{
	return false;
}
