#include "characterstartvalues.h"

void initValuesCharacters(Character * characterToInit, int posX, int posY, int damage, int speed, int health, bool isLive)
{
	characterToInit->pos_X = posX;
	characterToInit->pos_Y = posY;
	characterToInit->dmg = damage;
	characterToInit->speed = speed;
	characterToInit->health = health;
	characterToInit->isLive = isLive;
}
