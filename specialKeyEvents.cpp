#include "specialKeyEvents.h"

/// <summary>
/// Specials the key events.
/// </summary>
/// <param name="ev">The event</param>
/// <param name="redraw">The redraw menu.</param>
/// <param name="refreashMap">The refreash map.</param>
/// <param name="nS">The next shift.</param>
/// <param name="displayScrean">The display screan.</param>
void specialKeyEvents(ALLEGRO_EVENT ev, bool *redraw, bool *refreashMap, int *nS, kindOfScreen *displayScrean, bool *reloadGame)
{
	switch (ev.keyboard.keycode)
	{
	case ALLEGRO_KEY_ESCAPE:
		if (*displayScrean == HELP_MENU)
		{
			*nS = 0;
			*redraw = true;
			*displayScrean = START_MENU;
		}
		else if (*displayScrean == HELP2_MENU)
		{
			*nS = 0;
			*redraw = true;
			*displayScrean = PAUSE_SCREAN;
		}
		else if (*displayScrean == GAME_SCREAN)
		{
			*redraw = true;
			*nS = 0;
			*displayScrean = PAUSE_SCREAN;
		}
		else if (*displayScrean == PAUSE_SCREAN)
		{
			*displayScrean = GAME_SCREAN;
			*refreashMap = true;
		}
		else *displayScrean = CLOSE_GAME;
		break;
	case ALLEGRO_KEY_ENTER:
		if (*nS == 0)
		{
			if ((*displayScrean == PAUSE_SCREAN) ||(*displayScrean == START_MENU))
			{
				*displayScrean = GAME_SCREAN;
				*refreashMap = true;
			}
			else if (*displayScrean == GAME_OVER)
			{
				*reloadGame = true;
				*displayScrean = GAME_SCREAN;
				/**refreashMap = true;*/
			}
		}
		else if (*nS == 1)
		{
			if (*displayScrean == PAUSE_SCREAN)
			{
				*reloadGame = true;
				*displayScrean = GAME_SCREAN;
			}
			else if (*displayScrean == GAME_OVER)
				*displayScrean = CLOSE_GAME;
			else if (*displayScrean == START_MENU) 
				*displayScrean = HELP_MENU;
		}
		else if (*nS == 2)
		{
			if (*displayScrean == PAUSE_SCREAN)
			{
				*displayScrean = HELP2_MENU;
			}
			else if (*displayScrean != NO_CHANGE) *displayScrean = CLOSE_GAME;
		}
		else if (*nS == 3)
		{
			if (*displayScrean == PAUSE_SCREAN)
			{
				*displayScrean = CLOSE_GAME;
			}
		}
		break;
	}
}