#ifndef _ALLEGRO_LIB_NATIVE_
#define _ALLEGRO_LIB_NATIVE_

#include <allegro5\allegro.h>
#include <allegro5\allegro_native_dialog.h>

#endif

#ifndef _KIND_OF_SCREAN_H_
#define _KIND_OF_SCREAN_H_

#include "kindOfScrean.h"

#endif

void specialKeyEvents(ALLEGRO_EVENT ev, bool *redraw, bool *refreashMap, int *nS, kindOfScreen *displayScrean, bool *reloadGame);
