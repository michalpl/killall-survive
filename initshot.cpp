#include "initshot.h"

void initShot(Bullet bullets[], const short timerCounter, const short maxShots, const short lastSide, const short modulo)
{
	if ((timerCounter % modulo) == 0)
	{
		for (int shotsCounter = 0; shotsCounter < maxShots; shotsCounter++)
		{
			if (!bullets[shotsCounter].isLive)
			{
				bullets[shotsCounter].isLive = true;
				bullets[shotsCounter].side[lastSide] = true;
				break;
			}
		}
	}
}
