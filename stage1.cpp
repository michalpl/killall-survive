#include "stage1.h"

void Stage1::initStage(const int WINDOWWIDTH, const int WINDOWHEIGHT, bool *reload)
{
	initValuesCharacters(&this->enemy1, WINDOWWIDTH - 50, 25, 5, 5, 25, true);
	initValuesCharacters(&this->enemy2, 0, WINDOWHEIGHT - 75, 5, 3, 35, true);

	initValuesBullet(this->bullet1, this->enemy1.pos_X + 25, this->enemy1.pos_Y + 25, false, 7, 10, 20);
	initValuesBullet(this->bullet2, this->enemy2.pos_X + 25, this->enemy2.pos_Y + 25, false, 7, 10, 20);

	if (*reload)
	{
		this->enemy1.graphics[0] = al_load_bitmap("data/enemysTexturs/stage1/characterDown.png");
		this->enemy1.graphics[1] = al_load_bitmap("data/enemysTexturs/stage1/characterDownHit.png");

		this->enemy2.graphics[0] = al_load_bitmap("data/enemysTexturs/stage1/characterUp.png");
		this->enemy2.graphics[1] = al_load_bitmap("data/enemysTexturs/stage1/characterUpHit.png");

		al_convert_mask_to_alpha(this->enemy1.graphics[0], al_map_rgb(0, 0, 255));
		al_convert_mask_to_alpha(this->enemy2.graphics[0], al_map_rgb(0, 0, 255));
		al_convert_mask_to_alpha(this->enemy1.graphics[1], al_map_rgb(0, 0, 255));
		al_convert_mask_to_alpha(this->enemy2.graphics[1], al_map_rgb(0, 0, 255));
		*reload = false;
	}
}

void Stage1::draw()
{
	for (int shotsCounter = 0; shotsCounter < 20; shotsCounter++)
	{
		al_draw_filled_ellipse(this->bullet1[shotsCounter].pos_X, this->bullet1[shotsCounter].pos_Y, 5, 5, al_map_rgb(255, 0, 0));
		al_draw_filled_ellipse(this->bullet2[shotsCounter].pos_X, this->bullet2[shotsCounter].pos_Y, 5, 5, al_map_rgb(255, 0, 0));
	}

	if (this->enemy1.isLive)
	{
		if (!hit1)
			al_draw_bitmap(this->enemy1.graphics[0], this->enemy1.pos_X, this->enemy1.pos_Y, NULL);
		else
			al_draw_bitmap(this->enemy1.graphics[1], this->enemy1.pos_X, this->enemy1.pos_Y, NULL);
		hit1 = false;
	}
	if (this->enemy2.isLive)
	{
		if (!hit2)
			al_draw_bitmap(this->enemy2.graphics[0], this->enemy2.pos_X, this->enemy2.pos_Y, NULL);
		else
			al_draw_bitmap(this->enemy2.graphics[1], this->enemy2.pos_X, this->enemy2.pos_Y, NULL);
		hit2 = false;
	}
}

void Stage1::moves(short ** map, int WINDOWWIDTH, int WINDOWHEIGHT, int posX, int posY)
{
	for (int shotsCounter = 0; shotsCounter < 20; shotsCounter++)
	{
		bulletMoves(&this->bullet1[shotsCounter].isLive, &this->bullet1[shotsCounter].pos_X, &this->bullet1[shotsCounter].pos_Y,
			this->bullet1[shotsCounter].speed, map, WINDOWWIDTH, WINDOWHEIGHT, 50, 20, this->bullet1[shotsCounter].side, this->enemy1.pos_X + 25, this->enemy1.pos_Y + 25);

		bulletMoves(&this->bullet2[shotsCounter].isLive, &this->bullet2[shotsCounter].pos_X, &this->bullet2[shotsCounter].pos_Y,
			this->bullet2[shotsCounter].speed, map, WINDOWWIDTH, WINDOWHEIGHT, 50, 20, this->bullet2[shotsCounter].side, this->enemy2.pos_X + 25, this->enemy2.pos_Y + 25);
	}

	if (this->enemy1.health <= 0)
	{
		this->enemy1.isLive = false;
		this->enemy1.pos_X = -100;
		this->enemy1.pos_Y = -100;
	}
	if (this->enemy2.health <= 0)
	{
		this->enemy2.isLive = false;
		this->enemy2.pos_X = -100;
		this->enemy2.pos_Y = -100;
	}

	this->enemy1.timer++;
	this->enemy1.timer %= 15;

	this->enemy2.timer++;
	this->enemy2.timer %= 15;

	if (((this->enemy1.pos_X - 50) <= (posX + 50)) && ((this->enemy1.pos_X + 100) >= (posX)) && this->enemy1.isLive)
	{
		initShot(this->bullet1, this->enemy1.timer, 20, DOWN, 15);
	}
	else this->enemy1.timer = 8;

	if (((this->enemy2.pos_X - 50) <= (posX + 50)) && ((this->enemy2.pos_X + 100) >= (posX)) && this->enemy2.isLive)
	{
		initShot(this->bullet2, this->enemy2.timer, 20, UP, 15);
	}
	else enemy2.timer = 3;

	if (this->enemy1.isLive)
	{
		if (this->enemy1.pos_X >= (WINDOWWIDTH - 50)) enemySide1 = true;
		else if (this->enemy1.pos_X <= 1) enemySide1 = false;

		if (enemySide1) this->enemy1.pos_X -= this->enemy1.speed;
		else this->enemy1.pos_X += this->enemy1.speed;
	}

	if (this->enemy2.isLive)
	{
		if (this->enemy2.pos_X >= (WINDOWWIDTH - 50)) enemySide2 = true;
		else if (this->enemy2.pos_X <= 1) enemySide2 = false;

		if (enemySide2) this->enemy2.pos_X -= this->enemy2.speed;
		else this->enemy2.pos_X += this->enemy2.speed;
	}

}
// POZAMIENIAc TO NA KILKA OSOBNYCH FUKNCKI KIEDYS TAM
void Stage1::collisionsCharacterBulletsWithEnemys(int posX, int posY, bool *isLive, int dmg, bool *side)
{
	if (((posX >= this->enemy1.pos_X) && (posX <= this->enemy1.pos_X + 50)) && ((posY >= this->enemy1.pos_Y) && (posY <= this->enemy1.pos_Y + 50)))
	{
		this->enemy1.health -= dmg;
		*isLive = false;
		side[0] = false;
		side[1] = false;
		side[2] = false;
		side[3] = false;
		this->hit1 = true;
	}

	if (((posX >= this->enemy2.pos_X) && (posX <= this->enemy2.pos_X + 50)) && ((posY >= this->enemy2.pos_Y) && (posY <= this->enemy2.pos_Y + 50)))
	{
		this->enemy2.health -= dmg;
		*isLive = false;
		this->hit2 = true;
	}
}

void Stage1::collisionsEnemysBulletsWithCharacter(int * health, const int posX, const int posY)
{
	for (int shotsCounter = 0; shotsCounter < 20; shotsCounter++)
	{
		if (((posX <= this->bullet1[shotsCounter].pos_X) && (posX + 50 >= this->bullet1[shotsCounter].pos_X)) && ((posY <= this->bullet1[shotsCounter].pos_Y) && (posY + 50 >= this->bullet1[shotsCounter].pos_Y)))
		{
			*health -= bullet1[shotsCounter].dmg;
			bullet1[shotsCounter].isLive = false;
		}

		if (((posX <= this->bullet2[shotsCounter].pos_X) && (posX + 50 >= this->bullet2[shotsCounter].pos_X)) && ((posY <= this->bullet2[shotsCounter].pos_Y) && (posY + 50 >= this->bullet2[shotsCounter].pos_Y)))
		{
			*health -= bullet2[shotsCounter].dmg;
			bullet2[shotsCounter].isLive = false;
		}
	}
}

bool Stage1::end()
{
	if (!this->enemy1.isLive && !this->enemy2.isLive) return true;

	return false;
}
