#ifndef _ALLEGRO_LIB_NATIVE_
#define _ALLEGRO_LIB_NATIVE_

#include <allegro5\allegro.h>
#include <allegro5\allegro_native_dialog.h>

#endif

#ifndef _KEYS_TYPE_H_
#define _KEYS_TYPE_H_

#include "keystype.h"

#endif

#ifndef _COLLISION_PACK_H_
#define _COLLISION_PACK_H_

#include "collisionpack.h"

#endif

void bulletMapCollision(short **map, const int pos_x, int pos_y, const int textureSize, const int speed, bool *isLive, bool *side);
//void bulletMapCollisionY(int **map, const int pos_x, int pos_y, const int textureSize, const int speed, bool *isLive, bool *side);

void bulletMoves(bool *isLive, int *pos_X, int *pos_Y, const int speed, short **map, const int mapWidth, const int mapHeight, const int textureSize, const int maxShots, bool *side, const int character_X, const int character_Y);