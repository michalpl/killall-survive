#include "mapFunction.h"

Graphics maps;																					//	ZMIENIC TO KIEDYS

/// <summary>
/// Loads the graphics for map.
/// </summary>
void MapFunction::loadGraphicMap()
{																		
	maps.mapKind[0] = al_load_bitmap("data/mapTexturs/map0.png");
	maps.mapKind[1] = al_load_bitmap("data/mapTexturs/map1.png");
	maps.mapKind[2] = al_load_bitmap("data/mapTexturs/map2.png");
	maps.mapKind[3] = al_load_bitmap("data/mapTexturs/map3.png");
	maps.mapKind[4] = al_load_bitmap("data/mapTexturs/map4.png");
	maps.mapKind[5] = al_load_bitmap("data/mapTexturs/map5.png");

	al_convert_mask_to_alpha(maps.mapKind[5], al_map_rgb(0, 0, 255));

	if (checkBitmapErr(maps.mapKind[0], "map0.png") == -1) cout << "map0.png Error" << endl;
	if (checkBitmapErr(maps.mapKind[1], "map1.png") == -1) cout << "map1.png Error" << endl;
	if (checkBitmapErr(maps.mapKind[2], "map2.png") == -1) cout << "map2.png Error" << endl;
	if (checkBitmapErr(maps.mapKind[3], "map3.png") == -1) cout << "map3.png Error" << endl;
	if (checkBitmapErr(maps.mapKind[4], "map4.png") == -1) cout << "map4.png Error" << endl;
	if (checkBitmapErr(maps.mapKind[4], "map5.png") == -1) cout << "map5.png Error" << endl;
}

/// <summary>
/// Loads the map from file to matrix.
/// </summary>
/// <param name="fileName">Name of the file.</param>
/// <param name="map">The map.</param>
void MapFunction::loadMap(const char *fileName, short **map)
{
	int loadMapCounterX = 0, loadMapCounterY = 0;
	int mapSizeX, mapSizeY;

	fstream file(fileName);

	if (file.is_open())
	{
		file >> mapSizeX >> mapSizeY;
		while (!file.eof())
		{
			file >> map[loadMapCounterX][loadMapCounterY];
			loadMapCounterX++;

			if (loadMapCounterX == mapSizeX)
			{
				loadMapCounterX = 0;
				loadMapCounterY++;
			}
		}
	}
}

/// <summary>
/// Draws the map.
/// </summary>
/// <param name="map">The map.</param>
/// <param name="lengthX">The length x.</param>
/// <param name="lengthY">The length y.</param>
void MapFunction::drawMap(short *map[], int lengthX, int lengthY)
{
	for (int i = 0; i < lengthX; i++)
	{
		for (int j = 0; j < lengthY; j++)
		{
			if (map[i][j] == 0)
				al_draw_bitmap(maps.mapKind[0], i * 50, j * 50, 0);
			else if (map[i][j] == 1)
				al_draw_bitmap(maps.mapKind[1], i * 50, j * 50, 0);
			else if (map[i][j] == 2)
				al_draw_bitmap(maps.mapKind[2], i * 50, j * 50, 0);
			else if (map[i][j] == 3)
				al_draw_bitmap(maps.mapKind[3], i * 50, j * 50, 0);
			else if (map[i][j] == 4)
				al_draw_bitmap(maps.mapKind[4], i * 50, j * 50, 0);
			else if (map[i][j] == 5)
			{
				al_draw_bitmap(maps.mapKind[1], i * 50, j * 50, 0);
				al_draw_bitmap(maps.mapKind[5], i * 50, j * 50, 0);
			}
		}
	}
}

/// <summary>
/// Displays the map.
/// </summary>
/// <param name="fileName">Name of the file.</param>
/// <param name="map">The map.</param>
/// <param name="reload">if set to <c>true</c> [reload] from file.</param>
/// <param name="refreashMap">The refreash map.</param>
/// <param name="loadGraphicMap">if set to <c>true</c> [load graphics for map].</param>
void MapFunction::displayMap(const short curentStage, short ** map, bool reload, bool *refreashMap, bool loadGraphicMap)
{
	string mapFileName = "data/mapBin/map" + to_string(curentStage) + ".txt";

	if (reload) this->loadMap(mapFileName.c_str(), map);
	if (loadGraphicMap) this->loadGraphicMap();

	if (*refreashMap)
	{
		this->drawMap(map, 17, 11);
		refreashMap = false;
	}
}
