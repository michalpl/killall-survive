#ifndef _ALLEGRO_LIB_NATIVE_
#define _ALLEGRO_LIB_NATIVE_

#include <allegro5\allegro.h>
#include <allegro5\allegro_native_dialog.h>

#endif

#ifndef _ALLEGRO_IMAGE_PRIMITIVES_
#define _ALLEGRO_IMAGE_PRIMITIVES_

#include <allegro5\allegro_image.h>
#include <allegro5\allegro_primitives.h> 

#endif

#ifndef _KIND_OF_SCREAN_H_
#define _KIND_OF_SCREAN_H_

#include "kindOfScrean.h"

#endif

#include "menuNavigation.h"

void drawStartScrean(ALLEGRO_BITMAP *bitmap, int sX1, int sY1, int sX2, int sY2, int nS, int sS);
void drawHelpScrean(ALLEGRO_BITMAP *bitmap, kindOfScreen *displayScrean);