#ifndef _ALLEGRO_LIB_NATIVE_
#define _ALLEGRO_LIB_NATIVE_

#include <allegro5\allegro.h>
#include <allegro5\allegro_native_dialog.h>

#endif

#ifndef _IOSTREAM_STRING_LIB_
#define _IOSTREAM_STRING_LIB_

#include <iostream>
#include <string>
using namespace std;

#endif

int checkDisplayErr(ALLEGRO_DISPLAY *display);
int checkBitmapErr(ALLEGRO_BITMAP *bitmap, string name);