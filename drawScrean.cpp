#include "drawScrean.h"

/// <summary>
/// Draws the start screan with selection rectangle.
/// </summary>
/// <param name="bitmap">The bitmap.</param>
/// <param name="sX1">The selection x1.</param>
/// <param name="sY1">The selection y1.</param>
/// <param name="sX2">The selection x2.</param>
/// <param name="sY2">The selection y2.</param>
/// <param name="nS">The next selection.</param>
/// <param name="sS">The shift selection.</param>
void drawStartScrean(ALLEGRO_BITMAP *bitmap, int sX1, int sY1, int sX2, int sY2, int nS, int sS)
{
	int newSelectionY = nS * sS;
	al_draw_bitmap(bitmap, 0, 0, 0);
	al_draw_rectangle(sX1, sY1 + newSelectionY, sX2, sY2 + newSelectionY, al_map_rgb(255, 0, 0), 1);
}

/// <summary>
/// Draws the help screan.
/// </summary>
/// <param name="bitmap">The bitmap.</param>
/// <param name="displayScrean">The display screan.</param>
void drawHelpScrean(ALLEGRO_BITMAP *bitmap, kindOfScreen *displayScrean)
{
		//al_clear_to_color(al_map_rgb_f(0.5, 0.5, 0.5));
		al_draw_bitmap(bitmap, 0, 0, 0);
}