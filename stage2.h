#ifndef _BULLET_H_
#define _BULLET_H_

#include "bullet.h"

#endif

#ifndef _CHARACTER_H_
#define _CHARACTER_H_

#include "character.h"

#endif

#ifndef _ALLEGRO_LIB_NATIVE_
#define _ALLEGRO_LIB_NATIVE_

#include <allegro5\allegro.h>
#include <allegro5\allegro_native_dialog.h>

#endif

#ifndef _ALLEGRO_IMAGE_PRIMITIVES_
#define _ALLEGRO_IMAGE_PRIMITIVES_

#include <allegro5\allegro_image.h>
#include <allegro5\allegro_primitives.h> 

#endif

#ifndef _INIT_SHOT_H_
#define _INIT_SHOT_H_

#include "initshot.h"

#endif

#ifndef _START_VALUES_
#define _START_VALUES_

#include "characterstartvalues.h"
#include "bulletstartvalues.h"

#endif

#ifndef _GRAPHIC_STRUCT_
#define _GRAPHIC_STRUCT_

#include "graphics.h";
#include "checkError.h"

#endif

struct Stage2
{
	Character enemy[3];

	bool enemySide1[3][4];

	bool hit[3];

	bool jump[3];

	void initStage(const int WINDOWWIDTH, const int WINDOWHEIGHT, bool *reload);
	void draw();
	void moves(int WINDOWWIDTH, int WINDOWHEIGHT, int posX, int posY);
	void collisionsCharacterBulletsWithEnemys(int posX, int posY, bool *isLive, int dmg, bool *side);
	void collisionsEnemysWithCharacter(int *health, const int posX, const int posY);
	bool end();
};