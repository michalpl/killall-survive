#include "menuNavigation.h"

/// <summary>
/// Menus the navigation.
/// </summary>
/// <param name="keyState">State of the key.</param>
/// <param name="nS">The next shift.</param>
/// <param name="redraw">The redraw.</param>
void menuNavigation(ALLEGRO_KEYBOARD_STATE keyState, int *nS, bool *redraw, const short number)
{
	if (al_key_down(&keyState, ALLEGRO_KEY_DOWN))
		*nS = (*nS + 1) % number;
	else if (al_key_down(&keyState, ALLEGRO_KEY_UP))
	{
		*nS = ((*nS - 1) % number);
		if (*nS < 0) *nS += number;
	}

	*redraw = true;
}