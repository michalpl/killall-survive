#ifndef _IOSTREAM_STRING_LIB_
#define _IOSTREAM_STRING_LIB_

#include <iostream>
#include <string>
using namespace std;

#endif

#ifndef _ALLEGRO_LIB_NATIVE_
#define _ALLEGRO_LIB_NATIVE_

#include <allegro5\allegro.h>
#include <allegro5\allegro_native_dialog.h>

#endif

#ifndef _GRAPHIC_STRUCT_
#define _GRAPHIC_STRUCT_

#include "graphics.h";
#include "checkError.h"

#endif

#ifndef _FSTREAM_LIB_
#define _FSTREAM_LIB_

#include <fstream>

#endif

struct MapFunction
{
	void loadGraphicMap();
	void loadMap(const char *fileName, short **map);
	void drawMap(short *map[], int lengthX, int lengthY);
	void displayMap(const short curentStage, short **map, bool reload, bool *refreashMap, bool loadGraphicMap);
};