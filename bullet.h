#include "bulletsmove.h"

struct Bullet
{
	int pos_X;
	int pos_Y;
	bool isLive = false;
	bool side[4] = { false, false, false, false };
	int speed;
	int dmg;
};