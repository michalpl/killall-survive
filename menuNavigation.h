#ifndef _ALLEGRO_LIB_NATIVE_
#define _ALLEGRO_LIB_NATIVE_

#include <allegro5\allegro.h>
#include <allegro5\allegro_native_dialog.h>

#endif

void menuNavigation(ALLEGRO_KEYBOARD_STATE keyState, int *nS, bool *redraw, const short number);
