#include "drawScrean.h"
#include "specialKeyEvents.h"
#include "mapFunction.h"
#include "stage1.h"
#include "stage2.h"

#define WINDOWWIDTH 850
#define	WINDOWHEIGHT 550

#define FPS 60

int main()
{
	ALLEGRO_DISPLAY *window = NULL;
	ALLEGRO_EVENT_QUEUE *event_queue = NULL;
	ALLEGRO_KEYBOARD_STATE keyState;
	ALLEGRO_TIMER *timer;

	MapFunction mapfunction;

	if (!al_init())
	{
		al_show_native_message_box(NULL, "ALLEGRO-ERR", "Error:", "You hahe a big problem ;)", NULL, ALLEGRO_MESSAGEBOX_ERROR);

		return 0;
	}
	else window = al_create_display(WINDOWWIDTH, WINDOWHEIGHT);

	if (checkDisplayErr(window) == -1) return -1;

	/*
		�ADOWANIE GRAFIK MENU (bitmapa do buffora)
	*/
	al_init_image_addon();

	Graphics graphic;

	graphic.helpScrean = al_load_bitmap("data/screans/helpscrean.png");
	if (checkBitmapErr(graphic.helpScrean, "helpscrean.png") == -1) return -1;
	
	graphic.firstMenu = al_load_bitmap("data/screans/firstmenu.png");
	if (checkBitmapErr(graphic.firstMenu, "firstmenu.png") == -1) return -1;

	graphic.pauseScrean = al_load_bitmap("data/screans/pause.png");
	if (checkBitmapErr(graphic.pauseScrean, "pause.png") == -1) return -1;

	graphic.gameOverScrean = al_load_bitmap("data/screans/gameover.png");
	if (checkBitmapErr(graphic.pauseScrean, "gameover.png") == -1) return -1;
	
	/*
		TWORZENIE EVENT�W 
	*/
	timer = al_create_timer(1.0 / FPS);
	event_queue = al_create_event_queue();
	al_install_keyboard();
	
	al_register_event_source(event_queue, al_get_display_event_source(window));
	al_register_event_source(event_queue, al_get_timer_event_source(timer));
	al_register_event_source(event_queue, al_get_keyboard_event_source());

	/*
		ZMIENNE POTRZEBNE DO PORUSZANIA SI� PO MENU
	*/
	const int selectionX1 = 90;
	const int selectionX2 = 290;
	const int selectionY1 = 165;
	const int selectionY2 = 229;

	const int shiftSelectionY = 64;

	int numberOfShifts = 0;
	int newSelectionY = 0;

	int maxChose;
	/*
		ZMIENNE WYSWIETLANIA SCREANOW
	*/
	kindOfScreen displayScrean = START_MENU;

	bool redrawStart = false;
	bool refreashMap = true;

	bool reloadGraphicMap = true;
	bool reloadFile = true;

	/*
		KLAWISZE DO PORUSZANIA SI� W GRZE
	*/
	bool key_down[9] = { false, false, false, false, false, false, false, false, false };
	int lastSide = RIGHT;
	short timerCounter = 0;

	/*
		LOGIKA POZIOM�W
	*/
	Stage1 stage1;
	Stage2 stage2;

	bool reloadEnemyFile = true;
	/*
		POSTAC GLOWNA INIT
	*/
	const int characterSize = 40;
	Character mainCharacter;

	graphic.charcter[0] = al_load_bitmap("data/mainCharacter/characterUp.png");
	graphic.charcter[1] = al_load_bitmap("data/mainCharacter/characterDown.png");
	graphic.charcter[2] = al_load_bitmap("data/mainCharacter/characterLeft.png");
	graphic.charcter[3] = al_load_bitmap("data/mainCharacter/characterRight.png");

	if (checkBitmapErr(graphic.charcter[0], "characterUp.png") == -1) return -1;
	if (checkBitmapErr(graphic.charcter[1], "characterDown.png") == -1) return -1;
	if (checkBitmapErr(graphic.charcter[2], "characterLeft.png") == -1) return -1;
	if (checkBitmapErr(graphic.charcter[3], "characterRight.png") == -1) return -1;

	al_convert_mask_to_alpha(graphic.charcter[0], al_map_rgb(0, 0, 255));
	al_convert_mask_to_alpha(graphic.charcter[1], al_map_rgb(0, 0, 255));
	al_convert_mask_to_alpha(graphic.charcter[2], al_map_rgb(0, 0, 255));
	al_convert_mask_to_alpha(graphic.charcter[3], al_map_rgb(0, 0, 255));
	
	/*
		POCISKI INIT
	*/
	const short maxShots = 20;
	
	Bullet *bullets = new Bullet[maxShots];

	/*
		STWORZENIE TABLICY 2D DLA MAPY
	*/
	short** map = new short*[17];
	for (short i = 0; i < 17; i++) map[i] = new short[11];
	
	short curentStage = 1;
	short lastStage = curentStage;
	short nextStage = curentStage;
	bool stageStart = true;
	bool reloadGame = true;

	string mapFileName;

	mapfunction.loadMap("data/mapBin/map1.txt", map);

	/*
		PIERWSZE WYRYSOWANIE MENU
	*/
	al_init_primitives_addon();

	drawStartScrean(graphic.firstMenu, selectionX1, selectionY1, selectionX2, selectionY2, 0, 0);

	al_flip_display();

	/*
		G��WNY KOD
	*/
	al_start_timer(timer);
	while (displayScrean != CLOSE_GAME)
	{
		ALLEGRO_EVENT ev;
		al_wait_for_event(event_queue, &ev);
		
		if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) displayScrean = CLOSE_GAME;
		
		/*
			ZDARZENIA NA KLAWISZE
		*/
		if ((ev.type == ALLEGRO_EVENT_KEY_DOWN))
		{
			specialKeyEvents(ev, &redrawStart, &refreashMap, &numberOfShifts, &displayScrean, &reloadGame);
			
			if (displayScrean == GAME_SCREAN)
			{
				keys_down(ev, key_down, &refreashMap);
				refreashMap = true;
			}
		}

		if ((ev.type == ALLEGRO_EVENT_KEY_UP))
		{
			keys_up(ev, key_down);
			timerCounter = 10;
		}
		
		/*
			PORUSZANIE SI� PO MENU
		*/
		if ((ev.type == ALLEGRO_EVENT_KEY_CHAR) && ((displayScrean == START_MENU) || (displayScrean == PAUSE_SCREAN) || (displayScrean == GAME_OVER)))
		{
			al_get_keyboard_state(&keyState);
			
			if (displayScrean == START_MENU) maxChose = 3;
			else if (displayScrean == PAUSE_SCREAN) maxChose = 4;
			else if (displayScrean == GAME_OVER) maxChose = 2;

			menuNavigation(keyState, &numberOfShifts, &redrawStart, maxChose);
		}

		/*
			WYSWIETLANIE ZAZNACZENIA
		*/
		if (redrawStart && (displayScrean == START_MENU))
		{
			drawStartScrean(graphic.firstMenu, selectionX1, selectionY1, selectionX2, selectionY2, numberOfShifts, shiftSelectionY);
			redrawStart = false;
		}
		/*
			WYWIETLENIE PAUZY
		*/
		else if (redrawStart && (displayScrean == PAUSE_SCREAN))
		{
			drawStartScrean(graphic.pauseScrean, selectionX1, selectionY1, selectionX2, selectionY2, numberOfShifts, shiftSelectionY);
			redrawStart = false;
		}

		/*
			WYWIETLENIE MENU HELP
		*/
		else if ((displayScrean == HELP_MENU) || (displayScrean == HELP2_MENU)) drawHelpScrean(graphic.helpScrean, &displayScrean);

		else if (redrawStart && (displayScrean == GAME_OVER))
		{
			drawStartScrean(graphic.gameOverScrean, selectionX1, selectionY1, selectionX2, selectionY2, numberOfShifts, shiftSelectionY);
			redrawStart = false;
		}

		/*
			WYSWIETLENIE GRY
		*/
		else if (displayScrean == GAME_SCREAN)
		{
			if (reloadGame)
			{
				initValuesCharacters(&mainCharacter, 1, WINDOWHEIGHT / 2 - characterSize / 2, 10, 3, 100, true);
				initValuesBullet(bullets, mainCharacter.pos_X + (characterSize / 2), mainCharacter.pos_Y + (characterSize / 2), false, 7, mainCharacter.dmg, maxShots);

				reloadGame = false;

				reloadFile = true;
				refreashMap - true;
				stageStart = true;

				curentStage = 1;
				lastStage = 1;

				stage1.initStage(WINDOWWIDTH, WINDOWHEIGHT, &reloadEnemyFile);

				reloadGame = false;
			}
			else if (curentStage != lastStage)
			{
				initValuesCharacters(&mainCharacter, 1, WINDOWHEIGHT / 2 - characterSize / 2, mainCharacter.dmg, mainCharacter.speed, mainCharacter.health, true);
				initValuesBullet(bullets, mainCharacter.pos_X + (characterSize / 2), mainCharacter.pos_Y + (characterSize / 2), false, 7, mainCharacter.dmg, maxShots);

				reloadFile = true;
				refreashMap - true;
				stageStart = true;
				reloadEnemyFile = true;

				if (curentStage == 2)
				{
					reloadEnemyFile - true;
					stage2.initStage(WINDOWWIDTH, WINDOWHEIGHT, &reloadEnemyFile);
				}

				lastStage = curentStage;
			}

			mapfunction.displayMap(curentStage, map, reloadFile, &refreashMap, reloadGraphicMap);

			if (!stageStart) map[16][5] = 0;

			for (int shotsCounter = 0; shotsCounter < maxShots; shotsCounter++)
			{
				/*if (!(bullets[shotsCounter].pos_X == mainCharacter.pos_X + 25) && !(bullets[shotsCounter].pos_Y == mainCharacter.pos_Y + 25))*/
				al_draw_filled_ellipse(bullets[shotsCounter].pos_X, bullets[shotsCounter].pos_Y, 5, 5, al_map_rgb(255, 255, 0));
			}

			if (key_down[UP]) lastSide = UP;
			else if (key_down[DOWN]) lastSide = DOWN;
			else if (key_down[LEFT]) lastSide = LEFT;
			else if (key_down[RIGHT]) lastSide = RIGHT;

			/*
				PRZECIWNIK
			*/
			if (stageStart)
			{
				if (curentStage == 1)
				{
					if (stage1.end()) stageStart = false;
					else stage1.draw();
				}
				else if (curentStage == 2)
				{
					stage2.draw();
				}
			}

			if (mainCharacter.isLive)
				al_draw_bitmap(graphic.charcter[lastSide], mainCharacter.pos_X, mainCharacter.pos_Y, NULL);

			al_draw_filled_rectangle(5, 5, mainCharacter.health + 5, 20, al_map_rgb(255, 0, 0));
			al_draw_rectangle(5, 5, 105, 20, al_map_rgb(255, 0, 0), NULL);

			reloadFile = false;
			reloadGraphicMap = false;

			al_flip_display();
		}

		/*
			RUCH POSTACI I POCISK�W
		*/
		if ((ev.type == ALLEGRO_EVENT_TIMER) && (displayScrean == GAME_SCREAN))
		{
			if (key_down[UP] || key_down[DOWN] || key_down[LEFT] || key_down[RIGHT]) timerCounter++;
			else
			{
				if (timerCounter < 15) timerCounter++;
			};
			
			timerCounter %= 20;

			for (int shotsCounter = 0; shotsCounter < maxShots; shotsCounter++)
			{
				bulletMoves(&bullets[shotsCounter].isLive, &bullets[shotsCounter].pos_X, &bullets[shotsCounter].pos_Y,
					bullets[shotsCounter].speed, map, WINDOWWIDTH, WINDOWHEIGHT, 50, maxShots, bullets[shotsCounter].side, mainCharacter.pos_X + (characterSize / 2), mainCharacter.pos_Y + (characterSize /2));

				if (curentStage == 1)
				{
					stage1.collisionsCharacterBulletsWithEnemys(bullets[shotsCounter].pos_X, bullets[shotsCounter].pos_Y, &bullets[shotsCounter].isLive, bullets[shotsCounter].dmg, bullets[shotsCounter].side);
				}
				else if (curentStage == 2)
				{
					stage2.collisionsCharacterBulletsWithEnemys(bullets[shotsCounter].pos_X, bullets[shotsCounter].pos_Y, &bullets[shotsCounter].isLive, bullets[shotsCounter].dmg, bullets[shotsCounter].side);
				}
			}

			if (curentStage == 1)
			{
				stage1.collisionsEnemysBulletsWithCharacter(&mainCharacter.health, mainCharacter.pos_X, mainCharacter.pos_Y);
				stage1.moves(map, WINDOWWIDTH, WINDOWHEIGHT, mainCharacter.pos_X, mainCharacter.pos_Y);
			}
			else if (curentStage == 2)
			{
				stage2.collisionsEnemysWithCharacter(&mainCharacter.health, mainCharacter.pos_X, mainCharacter.pos_Y);
				stage2.moves(WINDOWWIDTH, WINDOWHEIGHT, mainCharacter.pos_X, mainCharacter.pos_Y);
			}

			initShot(bullets, timerCounter, maxShots, lastSide, 20);

			if (mainCharacter.health <= 0)
			{
				mainCharacter.isLive = false;
				redrawStart = true;
				curentStage = 1;
				lastStage = 0;
				numberOfShifts = 0;
				displayScrean = GAME_OVER;
			}
			/*
				RUCH POSTACI
			*/
			if(mainCharacter.isLive)
				characterMoves(key_down, &mainCharacter.pos_X, &mainCharacter.pos_Y, mainCharacter.speed, map, WINDOWWIDTH, WINDOWHEIGHT, 50, &curentStage, characterSize);
		}
		al_flip_display();
	}
	al_stop_timer(timer);

	al_destroy_timer(timer);
	al_destroy_event_queue(event_queue);
	al_destroy_bitmap(graphic.firstMenu);
	al_destroy_bitmap(graphic.helpScrean);
	al_destroy_bitmap(graphic.pauseScrean);
	al_destroy_display(window);

	delete[] map;
	delete[] bullets;

	return 0;
}