#include "charactermove.h"

/// <summary>
/// Keys_downs the specified ev.
/// </summary>
/// <param name="ev">The ev.</param>
/// <param name="key_down">The key_down.</param>
/// <param name="refreashMap">The refreash map.</param>
void keys_down(ALLEGRO_EVENT ev, bool *key_down, bool *refreashMap)
{
	switch (ev.keyboard.keycode)
	{
	case ALLEGRO_KEY_W:
		key_down[W] = true;
		*refreashMap = true;
		break;
	case ALLEGRO_KEY_S:
		key_down[S] = true;
		*refreashMap = true;
		break;
	case ALLEGRO_KEY_A:
		key_down[A] = true;
		*refreashMap = true;
		break;
	case ALLEGRO_KEY_D:
		key_down[D] = true;
		*refreashMap = true;
		break;
	case ALLEGRO_KEY_UP:
		key_down[UP] = true;
		key_down[DOWN] = false;
		key_down[LEFT] = false;
		key_down[RIGHT] = false;
		break;
	case ALLEGRO_KEY_DOWN:
		key_down[UP] = false;
		key_down[DOWN] = true;
		key_down[LEFT] = false;
		key_down[RIGHT] = false;
		break;
	case ALLEGRO_KEY_LEFT:
		key_down[UP] = false;
		key_down[DOWN] = false;
		key_down[LEFT] = true;
		key_down[RIGHT] = false;
		break;
	case ALLEGRO_KEY_RIGHT:
		key_down[UP] = false;
		key_down[DOWN] = false;
		key_down[LEFT] = false;
		key_down[RIGHT] = true;
		break;
	}
}

/// <summary>
/// Keys_ups the specified ev.
/// </summary>
/// <param name="ev">The ev.</param>
/// <param name="key_up">The key_up.</param>
void keys_up(ALLEGRO_EVENT ev, bool *key_up)
{
	switch (ev.keyboard.keycode)
	{
	case ALLEGRO_KEY_W:
		key_up[W] = false;
		break;
	case ALLEGRO_KEY_S:
		key_up[S] = false;
		break;
	case ALLEGRO_KEY_A:
		key_up[A] = false;
		break;
	case ALLEGRO_KEY_D:
		key_up[D] = false;
		break;
	case ALLEGRO_KEY_UP:
		key_up[UP] = false;
		break;
	case ALLEGRO_KEY_DOWN:
		key_up[DOWN] = false;
		break;
	case ALLEGRO_KEY_LEFT:
		key_up[LEFT] = false;
		break;
	case ALLEGRO_KEY_RIGHT:
		key_up[RIGHT] = false;
		break;
	}
}

/// <summary>
/// Collision with map when character moves along the x-axis.
/// </summary>
/// <param name="map">The map.</param>
/// <param name="pos_x">The pos_x.</param>
/// <param name="pos_y">The pos_y.</param>
/// <param name="textureSize">Size of the texture.</param>
/// <param name="speed">The speed.</param>
/// <param name="key_down">The key_down.</param>
void mapCollisionX(short **map, int *pos_x, int pos_y, const int textureSize, const int speed, bool *key_down, short *stage, const int characterSize)
{
	int x, y1, y2, y3;

	y1 = ((pos_y) / textureSize);										//	g�rny r�g
	y2 = ((pos_y + characterSize - 4) / textureSize);						//	dolny r�g - 4px
	y3 = ((pos_y + ((characterSize - 4) / 2)) / textureSize);				//	�rodek - 4px

	if (key_down[A])
		x = ((*pos_x) / textureSize);
	else if (key_down[D])
		x = ((*pos_x + characterSize - 4) / textureSize);

	if (collision(&map[x][y1], &map[x][y2], &map[x][y3], 1) && key_down[A]) *pos_x += speed;
	else if (collision(&map[x][y1], &map[x][y2], &map[x][y3], 1) && key_down[D]) *pos_x -= speed;
	else if (((map[x][y1] == 0) || (map[x][y2] == 0) || (map[x][y3] == 0))) *stage += 1;
}

/// <summary>
/// Collision with map when character moves along the y-axis.
/// </summary>
/// <param name="map">The map.</param>
/// <param name="pos_x">The pos_x.</param>
/// <param name="pos_y">The pos_y.</param>
/// <param name="textureSize">Size of the texture.</param>
/// <param name="speed">The speed.</param>
/// <param name="key_down">The key_down.</param>
void mapCollisionY(short **map, int pos_x, int *pos_y, const int textureSize, const int speed, bool *key_down, short *stage, const int characterSize)
{
	int x1, x2, x3, y;

	x1 = ((pos_x) / textureSize);										//	lewy r�g
	x2 = ((pos_x + characterSize - 4) / textureSize);						//	prawy r�g - 4px 
	x3 = ((pos_x + (characterSize - 4) / 2) / textureSize);				//	�rodek - 4px

	if (key_down[W])
		y = ((*pos_y) / textureSize);
	else if (key_down[S])
		y = ((*pos_y + characterSize - 4) / textureSize);

	if (collision(&map[x1][y], &map[x2][y], &map[x3][y], 1) && key_down[W]) *pos_y += speed;
	else if (collision(&map[x1][y], &map[x2][y], &map[x3][y], 1) && key_down[S]) *pos_y -= speed;
	else if (((map[x1][y] == 0) || (map[x2][y] == 0) || (map[x3][y] == 0))) *stage += 1;
}

/// <summary>
/// Moveses the specified key_down.
/// </summary>
/// <param name="key_down">The key_down.</param>
/// <param name="pos_x">The pos_x.</param>
/// <param name="pos_y">The pos_y.</param>
/// <param name="speed">The speed.</param>
/// <param name="map">The map.</param>
/// <param name="mapWidth">Width of the map.</param>
/// <param name="mapHeight">Height of the map.</param>
/// <param name="textureSize">Size of the texture.</param>
void characterMoves(bool *key_down, int *pos_x, int *pos_y, int speed, short **map, const int mapWidth, const int mapHeight, const int textureSize, short *stage, const int characterSize)
{
	if (key_down[W])
		if (*pos_y > 0)
		{
			*pos_y -= speed;
			mapCollisionY(map, *pos_x, pos_y, textureSize, speed, key_down, stage, characterSize);
		}
	if (key_down[S])
		if (*pos_y < mapHeight - textureSize)
		{
			*pos_y += speed;
			mapCollisionY(map, *pos_x, pos_y, textureSize, speed, key_down, stage, characterSize);
		}
	if (key_down[A])
		if (*pos_x > 0)
		{
			*pos_x -= speed;
			mapCollisionX(map, pos_x, *pos_y, textureSize, speed, key_down, stage, characterSize);
		}
	if (key_down[D])
		if (*pos_x < mapWidth - textureSize)
		{
			*pos_x += speed;
			mapCollisionX(map, pos_x, *pos_y, textureSize, speed, key_down, stage, characterSize);
		}
}