#include "charactermove.h"

#ifndef _ALLEGRO_LIB_NATIVE_
#define _ALLEGRO_LIB_NATIVE_

#include <allegro5\allegro.h>
#include <allegro5\allegro_native_dialog.h>

#endif

struct Character
{
	int pos_X;
	int pos_Y;
	int dmg;
	int health;
	int speed;
	int shotSpeed;
	int timer;
	bool isLive;
	ALLEGRO_BITMAP *graphics[4];

};