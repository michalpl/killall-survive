#include "bulletsmove.h"

void bulletMapCollision(short ** map, const int pos_x, int pos_y, const int textureSize, const int speed, bool *isLive, bool *side)
{
	int y = pos_y / textureSize;
	int x = pos_x / textureSize;

	if (collision(&map[x][y], &map[x][y], &map[x][y], 0))
	{
		*isLive = false;
		*side = false;
	}
}
//
//void bulletMapCollisionY(int ** map, const int pos_x, int pos_y, const int textureSize, const int speed, bool *isLive, bool *side)
//{
//	int y, x1, x2;
//
//	x1 = ((pos_x) / textureSize);											//	lewy r�g
//	x2 = ((pos_x) / textureSize);											//	prawy r�g
//	y = ((pos_y) / textureSize);
//
//	if (((map[x1][y] == 2) || (map[x2][y] == 2) || (map[pos_x / textureSize][y] == 2)))
//	{
//		*isLive = false;
//		*side = false;
//	}
//}

void bulletMoves(bool * isLive, int * pos_X, int * pos_Y, const int speed, short ** map, const int mapWidth, const int mapHeight, const int textureSize, const int maxShots, bool *side, const int character_X, const int character_Y)
{
	if (!*isLive)
	{
		*pos_X = character_X;
		*pos_Y = character_Y;
	}
	if (side[RIGHT] && *isLive)
	{
		*pos_X += speed;
		if (*pos_X > mapWidth - 5)
		{
			*isLive = false;
			side[RIGHT] = false;
		}
		else bulletMapCollision(map, *pos_X, *pos_Y, textureSize, speed, isLive, &side[RIGHT]);
	}
	else if (side[UP] && *isLive)
	{
		*pos_Y -= speed;
		if (*pos_Y < 0)
		{
			*isLive = false;
			side[UP] = false;
		}
		else bulletMapCollision(map, *pos_X, *pos_Y, textureSize, speed, isLive, &side[UP]);
	}
	else if (side[LEFT] && *isLive)
	{
		*pos_X -= speed;
		if (*pos_X < 0)
		{
			*isLive = false;
			side[LEFT] = false;
		}
		else bulletMapCollision(map, *pos_X, *pos_Y, textureSize, speed, isLive, &side[LEFT]);
	}
	else if (side[DOWN] && *isLive)
	{
		*pos_Y += speed;
		if (*pos_Y > mapHeight - 5)
		{
			*isLive = false;
			side[DOWN] = false;
		}
		else bulletMapCollision(map, *pos_X, *pos_Y, textureSize, speed, isLive, &side[DOWN]);
	}
	if (!*isLive)
	{
		*pos_X = character_X;
		*pos_Y = character_Y;
	}
}
