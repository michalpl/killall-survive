#ifndef _ALLEGRO_LIB_NATIVE_
#define _ALLEGRO_LIB_NATIVE_

#include <allegro5\allegro.h>
#include <allegro5\allegro_native_dialog.h>

#endif

#ifndef _KEYS_TYPE_H_
#define _KEYS_TYPE_H_

#include "keystype.h"

#endif

#ifndef _COLLISION_PACK_H_
#define _COLLISION_PACK_H_

#include "collisionpack.h"

#endif

void keys_down(ALLEGRO_EVENT ev, bool *key_down, bool *refreashMap);
void keys_up(ALLEGRO_EVENT ev, bool *key_up);
void mapCollisionX(short **map, int *pos_x, int pos_y, const int textureSize, const int speed, bool *key_down, short *stage, const int characterSize);
void mapCollisionY(short **map, int pos_x, int *pos_y, const int textureSize, const int speed, bool *key_down, short *stage, const int characterSize);

void characterMoves(bool *key_down, int *pos_x, int *pos_y, int speed, short **map, const int mapWidth, const int mapHeight, const int textureSize, short *stage, const int characterSize);
