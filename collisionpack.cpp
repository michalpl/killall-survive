#include "collisionpack.h"

void collisionInit(Collisions collisions[])
{
	collisions[0].bullet = false;
	collisions[0].character = false;
	collisions[0].enemy = false;

	collisions[1].bullet = false;
	collisions[1].character = false;
	collisions[1].enemy = false;

	collisions[2].bullet = true;
	collisions[2].character = true;
	collisions[2].enemy = false;

	collisions[3].bullet = false;
	collisions[3].character = false;
	collisions[3].enemy = false;

	collisions[4].bullet = true;
	collisions[4].character = true;
	collisions[4].enemy = true;

	collisions[5].bullet = false;
	collisions[5].character = true;
	collisions[5].enemy = true;
}

bool collision(short *kindOfTexture1, short *kindOfTexture2, short *kindOfTexture3, short type)
{
	Collisions collisionPack[6];

	collisionInit(collisionPack);
	
	if (type == 0)
	{
		if (collisionPack[*kindOfTexture1].bullet || collisionPack[*kindOfTexture2].bullet || collisionPack[*kindOfTexture3].bullet) return true;
		else return false;
	}
	else if (type == 1)
	{
		if (collisionPack[*kindOfTexture1].character || collisionPack[*kindOfTexture2].character || collisionPack[*kindOfTexture3].character) return true;
		else return false;
	}
	else if (type == 3)
	{
		if (collisionPack[*kindOfTexture1].enemy || collisionPack[*kindOfTexture2].enemy || collisionPack[*kindOfTexture3].enemy) return true;
		else return false;
	}
	
	return false;
}