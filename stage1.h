#ifndef _BULLET_H_
#define _BULLET_H_

#include "bullet.h"

#endif

#ifndef _CHARACTER_H_
#define _CHARACTER_H_

#include "character.h"

#endif

#ifndef _ALLEGRO_LIB_NATIVE_
#define _ALLEGRO_LIB_NATIVE_

#include <allegro5\allegro.h>
#include <allegro5\allegro_native_dialog.h>

#endif

#ifndef _ALLEGRO_IMAGE_PRIMITIVES_
#define _ALLEGRO_IMAGE_PRIMITIVES_

#include <allegro5\allegro_image.h>
#include <allegro5\allegro_primitives.h> 

#endif

#ifndef _INIT_SHOT_H_
#define _INIT_SHOT_H_

#include "initshot.h"

#endif

#ifndef _START_VALUES_
#define _START_VALUES_

#include "characterstartvalues.h"
#include "bulletstartvalues.h"

#endif

#ifndef _GRAPHIC_STRUCT_
#define _GRAPHIC_STRUCT_

#include "graphics.h";
#include "checkError.h"

#endif

struct Stage1
{
	Character enemy1;
	Character enemy2;
	Bullet bullet1[20];
	Bullet bullet2[20];

	bool enemySide1;
	bool enemySide2;
	bool hit1;
	bool hit2;

	void initStage(const int WINDOWWIDTH, const int WINDOWHEIGHT, bool *reload);
	void draw();
	void moves(short ** map, int WINDOWWIDTH, int WINDOWHEIGHT, int posX, int posY);
	void collisionsCharacterBulletsWithEnemys(int posX, int posY, bool *isLive, int dmg, bool *side);
	void collisionsEnemysBulletsWithCharacter(int *health, const int posX, const int posY);
	bool end();
};