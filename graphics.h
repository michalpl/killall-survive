#ifndef _ALLEGRO_LIB_NATIVE_
#define _ALLEGRO_LIB_NATIVE_

#include <allegro5\allegro.h>
#include <allegro5\allegro_native_dialog.h>

#endif

#ifndef _ALLEGRO_IMAGE_PRIMITIVES_
#define _ALLEGRO_IMAGE_PRIMITIVES_

#include <allegro5\allegro_image.h>
#include <allegro5\allegro_primitives.h> 

#endif

/// <summary>
/// Struct with kind of graphic. 
/// </summary>
struct Graphics
{
	ALLEGRO_BITMAP *mapKind[6];
	ALLEGRO_BITMAP *firstMenu;
	ALLEGRO_BITMAP *helpScrean;
	ALLEGRO_BITMAP *pauseScrean;
	ALLEGRO_BITMAP *gameOverScrean;
	ALLEGRO_BITMAP *charcter[4];
};