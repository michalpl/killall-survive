#include "bulletstartvalues.h"

void initValuesBullet(Bullet bulletToInit[], int posX, int posY, bool isLive, int speed, int damage, int maxShots)
{
	for (int shotsCounter = 0; shotsCounter < maxShots; shotsCounter++)
	{
		bulletToInit[shotsCounter].pos_X = posX;
		bulletToInit[shotsCounter].pos_Y = posY;
		bulletToInit[shotsCounter].isLive = isLive;
		bulletToInit[shotsCounter].side[0] = false;
		bulletToInit[shotsCounter].side[1] = false;
		bulletToInit[shotsCounter].side[2] = false;
		bulletToInit[shotsCounter].side[3] = false;
		bulletToInit[shotsCounter].speed = speed;
		bulletToInit[shotsCounter].dmg = damage;
	}
}
